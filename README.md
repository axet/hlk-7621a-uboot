U-Boot modification for MT7621 Devices
==========
Source from HiLink.

**Flashing the bootloader is risky.**

**If you like expensive bricks proceed without caution.**

**You will need a working Serial Console and a TFTP server.**

From the Serial Console (UART) : Option 9

Compile:
========

1) Debian 12
2) install gcc-mipsel-linux-gnu (gcc-12)
3) copy your config to .config
4) make menuconfig CONFIG_CROSS_COMPILER_PATH="$PWD/bin"
5) make
